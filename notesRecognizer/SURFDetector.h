//
//  SURFDetector.h
//  notesRecognizer
//
//  Created by Kean Ho Chew on 26/05/2016.
//  Copyright © 2016 Kean Ho Chew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <opencv2/opencv.hpp>
#import "ImageData.h"

@interface SURFDetector : NSObject

-(std::vector<cv::KeyPoint>)getKeypoint:(cv::Mat)greyscaleImage;

- (cv::Mat) getDescriptor:(cv::Mat)greyscaleImage
            keypoint:(std::vector<cv::KeyPoint>)keypoint;

- (void)processImageData:(ImageData *)image;

- (BOOL)detectSimilarity:(ImageData *)sceneImage
          referenceImage:(ImageData *)object
             outputImage:(cv::Mat *)image;

@end
