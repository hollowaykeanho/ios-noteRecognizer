//
//  ImageTool.h
//  notesRecognizer
//
//  Created by Kean Ho Chew on 19/05/2016.
//  Copyright © 2016 Kean Ho Chew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <opencv2/opencv.hpp>

@interface ImageTool : NSObject

+ (cv::Mat)cvMatFromUIImage:(UIImage *)image;
+ (UIImage *)UIImageFromCVMat:(cv::Mat)cvMat;

@end
