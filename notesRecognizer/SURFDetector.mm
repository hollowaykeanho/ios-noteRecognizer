//
//  SURFDetector.m
//  notesRecognizer
//
//  Created by Kean Ho Chew on 26/05/2016.
//  Copyright © 2016 Kean Ho Chew. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <opencv2/opencv.hpp>
#import <opencv2/nonfree/nonfree.hpp>
#import "SURFDetector.h"
#import "ImageData.h"
#import "ImageTool.h"

#ifdef __cplusplus

@interface SURFDetector()

@property int recognitionPassingLength;

@end

static cv::SurfFeatureDetector detector;

@implementation SURFDetector

- (instancetype)init
{
    self = [super init];
    detector = cv::SurfFeatureDetector(200);
    self.recognitionPassingLength = 70;
    return self;
}


-(std::vector<cv::KeyPoint>)getKeypoint:(cv::Mat)greyscaleImage
{
    std::vector<cv::KeyPoint> keypoint;
    detector.detect(greyscaleImage, keypoint);
    return keypoint;
}


- (cv::Mat)getDescriptor:(cv::Mat)greyscaleImage
                keypoint:(std::vector<cv::KeyPoint>)keypoint
{
    cv::SurfDescriptorExtractor extractor;
    cv::Mat descriptor;

    extractor.compute(greyscaleImage, keypoint, descriptor);
    return descriptor;
}

- (void)processImageData:(ImageData *)image
{
    if (!image.matImage.data)
        return;

    image.grayscaleMatImage = cv::Mat(image.matImage.rows, image.matImage.cols, CV_8UC1);
    cv::cvtColor(image.matImage, image.grayscaleMatImage, cv::COLOR_RGBA2GRAY, 1);
    image.keypoint = [self getKeypoint:image.grayscaleMatImage];
    image.descriptor = [self getDescriptor:image.grayscaleMatImage keypoint:image.keypoint];
}

- (BOOL)detectSimilarity:(ImageData *)scene
          referenceImage:(ImageData *)object
             outputImage:(cv::Mat *)image
{
    // input data validation
    if (!object.descriptor.data || !scene.descriptor.data)
        return NO;

    // Using FLANN to get matches between the scene and objects
    cv::FlannBasedMatcher matcher;
    std::vector<cv::DMatch> matches;
    matcher.match(object.descriptor, scene.descriptor, matches);
    std::vector<cv::DMatch> goodMatches = [self getGoodMatches:object.descriptor matches:matches];
    
    // extract sceneCorners
    std::vector<cv::Point2f> sceneCorners(4);
    sceneCorners = [self getHomographyImageCorners:goodMatches scene:scene object:object];
    
//    *image = [self generateSURFOutputView:scene referenceImage:object matches:goodMatches sceneCorners:sceneCorners];
//    *image = [self generateNormalOutputView:scene.matImage scenceCorners:sceneCorners];
//    *image = [scene grayscale];
    
    // Return Value
    return [self checkValidSURFDetection:sceneCorners];
}

#pragma mark - private methods
- (std::vector<cv::DMatch>) getGoodMatches:(cv::Mat)descriptor
                            matches:(std::vector<cv::DMatch>)matches
{
    int i = 0;
    double minDistance = 100, maxDistance = 0;
    std::vector<cv::DMatch> goodMatches;
    
    for(i=0; i<descriptor.rows; i++) {
        double dist = matches[i].distance;
        if (dist < minDistance)
            minDistance = dist;
        if (dist > maxDistance)
            maxDistance = dist;
    }

    for(i=0; i<descriptor.rows; i++) {
        if (matches[i].distance < 3*minDistance)
            goodMatches.push_back(matches[i]);
    }
    return goodMatches;
}


- (std::vector<cv::Point2f>)getHomographyImageCorners:(std::vector<cv::DMatch>)matches
                            scene:(ImageData *)scene
                            object:(ImageData *)object
{
    int i;
    std::vector<cv::Point2f> objectPoints;
    std::vector<cv::Point2f> scenePoints;
    std::vector<cv::Point2f> sceneCorners(4);
    std::vector<cv::Point2f> objectCorners(4);

    // preparing homography extraction
    for(i=0; i<matches.size(); i++) {
        objectPoints.push_back(object.keypoint[matches[i].queryIdx].pt);
        scenePoints.push_back(scene.keypoint[matches[i].trainIdx].pt);
    }
    if (!objectPoints.data() || !scenePoints.data())
        return sceneCorners;
    
    // homography extraction
    cv::Mat homography = cv::findHomography(objectPoints, scenePoints, CV_RANSAC);
    objectCorners[0] = cvPoint(0, 0);
    objectCorners[1] = cvPoint(object.matImage.cols, 0);
    objectCorners[2] = cvPoint(object.matImage.cols, object.matImage.rows);
    objectCorners[3] = cvPoint(0, object.matImage.rows);
    cv::perspectiveTransform(objectCorners, sceneCorners, homography);
    
    return sceneCorners;
}


- (BOOL) checkValidSURFDetection:(std::vector<cv::Point2f>)sceneCorners
{
    if (abs((int)sceneCorners[3].x - (int)sceneCorners[1].x) > self.recognitionPassingLength &&
        abs((int)sceneCorners[3].y - (int)sceneCorners[1].y) > self.recognitionPassingLength
        )
        return YES;
    
    return NO;
}


- (cv::Mat)generateSURFOutputView:(ImageData *)scene referenceImage:(ImageData *)object
                     matches:(std::vector<cv::DMatch>)matches sceneCorners:(std::vector<cv::Point2f>)sceneCorners
{
    cv::Mat outputImage;
    drawMatches(object.grayscaleMatImage, object.keypoint,
                scene.grayscaleMatImage, scene.keypoint,
                matches, outputImage,
                cv::Scalar::all(-1), cv::Scalar::all(-1),
                cv::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS
                );

    cv::line(outputImage,
             sceneCorners[0] + cv::Point2f(object.matImage.cols, 0),
             sceneCorners[1] + cv::Point2f(object.matImage.cols, 0),
             cv::Scalar(0, 255, 0), 4
             );

    cv::line(outputImage,
             sceneCorners[1] + cv::Point2f(object.matImage.cols, 0),
             sceneCorners[2] + cv::Point2f(object.matImage.cols, 0),
             cv::Scalar(0, 255, 0), 4
             );

    cv::line(outputImage,
             sceneCorners[2] + cv::Point2f(object.matImage.cols, 0),
             sceneCorners[3] + cv::Point2f(object.matImage.cols, 0),
             cv::Scalar(0, 255, 0), 4
             );

    cv::line(outputImage,
             sceneCorners[3] + cv::Point2f(object.matImage.cols, 0),
             sceneCorners[0] + cv::Point2f(object.matImage.cols, 0),
             cv::Scalar(0, 255, 0), 4
             );
    return outputImage;
}

- (cv::Mat)generateNormalOutputView:(cv::Mat)image scenceCorners:(std::vector<cv::Point2f>)sceneCorners
{
    cv::line(image, sceneCorners[0], sceneCorners[1], cv::Scalar(0, 255, 0), 4);
    cv::line(image, sceneCorners[1], sceneCorners[2], cv::Scalar(0, 255, 0), 4);
    cv::line(image, sceneCorners[2], sceneCorners[3], cv::Scalar(0, 255, 0), 4);
    cv::line(image, sceneCorners[3], sceneCorners[0], cv::Scalar(0, 255, 0), 4);
    return image;
}

#endif
@end
