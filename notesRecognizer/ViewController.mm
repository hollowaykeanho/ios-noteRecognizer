//
//  ViewController.m
//  notesRecognizer
//
//  Created by Kean Ho Chew on 12/05/2016.
//  Copyright © 2016 Kean Ho Chew. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <opencv2/highgui/cap_ios.h>
#import <opencv2/opencv.hpp>
#import <opencv2/features2d/features2d.hpp>
#import "ViewController.h"
#import "ImageTool.h"
#import "ImageData.h"
#import "SURFDetector.h"

#ifdef __cplusplus

@interface ViewController () <AVCaptureVideoDataOutputSampleBufferDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *recognizeButton;
@property AVCaptureSession *videoSession;
@property BOOL cameraStatus;
@property SURFDetector *surfDetector;
@property ImageData *rm100;
@property ImageData *rm10;
@property ImageData *rm50;
@property ImageData *rm20;
@property ImageData *rm5;
@property ImageData *rm1;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Setting video capture session
    self.videoSession = [[AVCaptureSession alloc] init];
    self.videoSession.sessionPreset = AVCaptureSessionPreset640x480;
    
    // Setting input for video capture
    NSArray *videoDevices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    for (AVCaptureDevice *device in videoDevices) {
        if (device.position == AVCaptureDevicePositionBack) {
            AVCaptureDeviceInput *cameraInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
            [self.videoSession addInput:cameraInput];
        }
    }

    // Setting output for video capture
    AVCaptureVideoDataOutput *captureOutput = [[AVCaptureVideoDataOutput alloc] init];
    dispatch_queue_t queue;
    queue = dispatch_queue_create("opencvMainEngine", DISPATCH_QUEUE_SERIAL);
    [captureOutput setSampleBufferDelegate:self queue:queue];
    NSDictionary *settings = @{(NSString *)kCVPixelBufferPixelFormatTypeKey:@(kCVPixelFormatType_32BGRA)};
    [captureOutput setVideoSettings:settings];
    captureOutput.alwaysDiscardsLateVideoFrames = YES;
    [self.videoSession addOutput:captureOutput];

    // Set Video Orientation
    AVCaptureConnection *videoConnection = NULL;
    for ( AVCaptureConnection *connection in [captureOutput connections] ) {
        for ( AVCaptureInputPort *port in [connection inputPorts] ) {
            if ( [[port mediaType] isEqual:AVMediaTypeVideo] ) {
                videoConnection = connection;
            }
        }
    }
    if([videoConnection isVideoOrientationSupported]) {
        [videoConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
    }


    // Setting other images and setup
    self.surfDetector = [[SURFDetector alloc] init];
    self.rm100 = [[ImageData alloc] init];
    self.rm100.matImage = [ImageTool cvMatFromUIImage:[UIImage imageNamed:@"rm100"]];
    [self.surfDetector processImageData:self.rm100];

    self.rm50 = [[ImageData alloc] init];
    self.rm50.matImage = [ImageTool cvMatFromUIImage:[UIImage imageNamed:@"rm50"]];
    [self.surfDetector processImageData:self.rm50];

    self.rm20 = [[ImageData alloc] init];
    self.rm20.matImage = [ImageTool cvMatFromUIImage:[UIImage imageNamed:@"rm20"]];
    [self.surfDetector processImageData:self.rm20];

    self.rm10 = [[ImageData alloc] init];
    self.rm10.matImage = [ImageTool cvMatFromUIImage:[UIImage imageNamed:@"rm10"]];
    [self.surfDetector processImageData:self.rm10];
    
    self.rm5 = [[ImageData alloc] init];
    self.rm5.matImage = [ImageTool cvMatFromUIImage:[UIImage imageNamed:@"rm5"]];
    [self.surfDetector processImageData:self.rm5];

    self.rm1 = [[ImageData alloc] init];
    self.rm1.matImage = [ImageTool cvMatFromUIImage:[UIImage imageNamed:@"rm1"]];
    [self.surfDetector processImageData:self.rm1];

    [self.videoSession startRunning];
    self.cameraStatus = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)didPressedButton:(id)sender {
    if (!self.cameraStatus) {
        [self.videoSession startRunning];
        self.cameraStatus = YES;
    } else {
        [self.videoSession stopRunning];
        self.cameraStatus = NO;
    }
    NSLog(@"Camera Status %d", self.cameraStatus);
}

#pragma mark - AVFoundation Video Camera
- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
       fromConnection:(AVCaptureConnection *)connection {
    NSString *reply = @"";
    cv::Mat outputImage;
    ImageData *scene = [[ImageData alloc] init];
    
    // Get a CMSampleBuffer's Core Video image buffer for the media data
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    
    // Lock the base address of the pixel buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    // Convert Pixel Data to cv::Mat
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    void *bufferAddress = CVPixelBufferGetBaseAddress(imageBuffer);
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    scene.matImage = cv::Mat((int)height, (int)width, CV_8UC4, bufferAddress, bytesPerRow).clone();
    cv::cvtColor(scene.matImage, scene.matImage, CV_BGRA2RGBA);
    [self.surfDetector processImageData:scene];

    // Process Image to surf data
    if ([self.surfDetector detectSimilarity:scene referenceImage:self.rm100 outputImage:&outputImage]) {
        reply = @"100";
    } else if ([self.surfDetector detectSimilarity:scene referenceImage:self.rm50 outputImage:&outputImage]) {
        reply = @"50";
    } else if ([self.surfDetector detectSimilarity:scene referenceImage:self.rm5 outputImage:&outputImage]) {
        reply = @"5";
    } else if ([self.surfDetector detectSimilarity:scene referenceImage:self.rm10 outputImage:&outputImage]) {
        reply = @"10";
    } else if ([self.surfDetector detectSimilarity:scene referenceImage:self.rm1 outputImage:&outputImage]) {
        reply = @"1";
    } else if ([self.surfDetector detectSimilarity:scene referenceImage:self.rm20 outputImage:&outputImage]) {
        reply = @"20";
    }

    if (![reply isEqualToString:@""]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (![reply isEqualToString:@""]) {
                if (self.presentedViewController != nil)
                    [self dismissViewControllerAnimated:YES completion:nil];
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:reply
                                                                               message:[NSString stringWithFormat:@"Recognizes %@", reply]
                                                                        preferredStyle:UIAlertControllerStyleAlert];

                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK"
                                                                 style:UIAlertActionStyleCancel
                                                               handler:nil];
                [alert addAction:cancel];
                [self presentViewController:alert animated:YES completion:nil];
            }
        });
    }

    // Unlock the pixel buffer
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);

    dispatch_async(dispatch_get_main_queue(), ^{
        self.imageView.image = [ImageTool UIImageFromCVMat:scene.matImage];
    });
}

#endif

@end
