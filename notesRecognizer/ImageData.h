//
//  ImageData.h
//  notesRecognizer
//
//  Created by Kean Ho Chew on 26/05/2016.
//  Copyright © 2016 Kean Ho Chew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <opencv2/opencv.hpp>


@interface ImageData : NSObject

@property cv::Mat matImage;
@property cv::Mat grayscaleMatImage;
@property std::vector<cv::KeyPoint> keypoint;
@property cv::Mat descriptor;

@end

